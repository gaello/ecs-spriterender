# Sprite Rendering with ECS in Unity

So you are looking for information on how to render sprites with ECS in Unity? That's easy! This repository contain what you looking for!

In addition to this repository I also made a post about it that you can find here: https://www.patrykgalach.com/2019/07/08/unity-ecs-sprite-rendering/

Enjoy!

---

# How to use it?

This repository contains an example of how you can render sprites with ECS in Unity.

If you want to see that implementation, go straight to [Assets/Scripts/](https://bitbucket.org/gaello/ecs-spriterender/src/master/Assets/Scripts/) folder. You will find all code that I wrote to make it work. Code also have comments so it would make a little bit more sense.

I hope you will enjoy it!

---

#Well done!

Now you know how to render sprites with ECS in Unity!

##Congratulations!

For more visit my blog: https://www.patrykgalach.com
