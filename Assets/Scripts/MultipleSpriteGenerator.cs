﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;

/// <summary>
/// Example of spawning Sprites using quad mesh and different materials.
/// </summary>
public class MultipleSpriteGenerator : MonoBehaviour
{
    // Amount of entities to spawn.
    [SerializeField]
    private int entitiesToSpawn = 10;

    // Reference to the sprite mesh - quad.
    [SerializeField]
    private Mesh spriteMesh;

    // References to materials with sprite texture.
    [SerializeField]
    private List<Material> spriteMaterials;

    /// <summary>
    /// Unity method called on the first frame.
    /// </summary>
    void Start()
    {
        GenerateEntities();
    }

    /// <summary>
    /// Generating example.
    /// </summary>
    public void GenerateEntities()
    {
        // Storing reference to entity manager.
        var entityManager = World.Active.EntityManager;

        // Creating temp array for entities.
        NativeArray<Entity> spriteEntities = new NativeArray<Entity>(entitiesToSpawn, Allocator.Temp);

        // Creating archetype for sprite.
        var spriteArchetype = entityManager.CreateArchetype(
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Translation)
            );

        // Creting entities.
        entityManager.CreateEntity(spriteArchetype, spriteEntities);

        // Creating Randomness.
        var rnd = new Unity.Mathematics.Random((uint)System.DateTime.UtcNow.Ticks);

        // Looping over entities
        for (int i = 0; i < entitiesToSpawn; i++)
        {
            var spriteEntity = spriteEntities[i];

            // Assigning values to the renderer.
            entityManager.SetSharedComponentData(spriteEntity, new RenderMesh { mesh = spriteMesh, material = spriteMaterials[rnd.NextInt(spriteMaterials.Count)] });

            // Assigning random position.
            entityManager.SetComponentData(spriteEntity, new Translation { Value = rnd.NextFloat3(new float3(-5, -3, 0), new float3(5, 3, 0)) });
        }

        // Clearing native array for entities.
        spriteEntities.Dispose();
    }
}
