﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;

/// <summary>
/// Example of spawning Sprites using quad mesh, material and list of sprites.
/// </summary>
public class SingleMatSpriteGenerator : MonoBehaviour
{
    // Amount of entities to spawn.
    [SerializeField]
    private int entitiesToSpawn = 10;

    // Reference to the sprite mesh - quad.
    [SerializeField]
    private Mesh spriteMesh;

    // Sprites from the atlas texture assigned in spriteMaterial.
    [SerializeField]
    private List<Sprite> sprites;

    // Reference to the material with sprite atlas texture.
    [SerializeField]
    private Material spriteMaterial;

    /// <summary>
    /// Unity method called on the first frame.
    /// </summary>
    void Start()
    {
        GenerateEntities();
    }

    /// <summary>
    /// Generating example.
    /// </summary>
    public void GenerateEntities()
    {
        // Storing reference to entity manager.
        var entityManager = World.Active.EntityManager;

        // Creating temp array for entities.
        NativeArray<Entity> spriteEntities = new NativeArray<Entity>(entitiesToSpawn, Allocator.Temp);

        // Creating archetype for sprite.
        var spriteArchetype = entityManager.CreateArchetype(
                typeof(RenderMesh),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(NonUniformScale)
            );

        // Creting entities.
        entityManager.CreateEntity(spriteArchetype, spriteEntities);

        // Creating dictionary with sprite materials.
        var spriteMaterials = new Dictionary<Sprite, Material>();

        // Generating sprite materials.
        for (int i = 0; i < sprites.Count; i++)
        {
            var spriteMat = Material.Instantiate(spriteMaterial);
            // Assigning sprite texture offset.
            spriteMat.mainTextureOffset = SpriteECSHelper.GetTextureOffset(sprites[i]);
            // Assigning sprite texture size.
            spriteMat.mainTextureScale = SpriteECSHelper.GetTextureSize(sprites[i]);

            // Assigning material name - you can skip it.
            spriteMat.name = sprites[i].name;

            // Adding material to dictionary.
            spriteMaterials[sprites[i]] = spriteMat;
        }

        // Creating Randomness.
        var rnd = new Unity.Mathematics.Random((uint)System.DateTime.UtcNow.Ticks);

        // Looping over entities
        for (int i = 0; i < entitiesToSpawn; i++)
        {
            var spriteEntity = spriteEntities[i];

            // Choosing random sprite to assign to entity.
            var sprite = sprites[rnd.NextInt(sprites.Count)];

            // Assigning values to the renderer.
            entityManager.SetSharedComponentData(spriteEntity, new RenderMesh { mesh = spriteMesh, material = spriteMaterials[sprite] });

            // Assigning random position.
            entityManager.SetComponentData(spriteEntity, new Translation { Value = rnd.NextFloat3(new float3(-5, -3, 0), new float3(5, 3, 0)) });

            // Assigning sprite size with pixels per unit measure.
            entityManager.SetComponentData(spriteEntity, new NonUniformScale { Value = SpriteECSHelper.GetQuadScale(sprite) });
        }

        // Clearing native array for entities.
        spriteEntities.Dispose();
        // Clearing material dictionary.
        spriteMaterials.Clear();
    }
}
