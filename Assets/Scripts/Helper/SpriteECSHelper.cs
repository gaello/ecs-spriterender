﻿using UnityEngine;
using Unity.Mathematics;

/// <summary>
/// Little helper class for getting data for Sprite rendering in ECS.
/// </summary>
public static class SpriteECSHelper
{
    /// <summary>
    /// Returns Texture Offset for provided sprite.
    /// </summary>
    /// <returns>The texture offset.</returns>
    /// <param name="sprite">Sprite.</param>
    public static Vector2 GetTextureOffset(Sprite sprite)
    {
        Vector2 offset = Vector2.Scale(sprite.rect.position, new Vector2(1f / sprite.texture.width, 1f / sprite.texture.height));
        return offset;
    }

    /// <summary>
    /// Returns Texture Size for provided sprite.
    /// </summary>
    /// <returns>The texture size.</returns>
    /// <param name="sprite">Sprite.</param>
    public static Vector2 GetTextureSize(Sprite sprite)
    {
        Vector2 size = Vector2.Scale(sprite.rect.size, new Vector2(1f / sprite.texture.width, 1f / sprite.texture.height));
        return size;
    }

    /// <summary>
    /// Returns quad scale for provided sprite. Uses Pixels per Unit measure.
    /// </summary>
    /// <returns>The quad scale.</returns>
    /// <param name="sprite">Sprite.</param>
    public static float3 GetQuadScale(Sprite sprite)
    {
        return new float3(sprite.rect.width / sprite.pixelsPerUnit, sprite.rect.height / sprite.pixelsPerUnit, 1);
    }
}
